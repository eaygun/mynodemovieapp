const user = require("../src/routes/controllers/authentication.controller");
const meal = require("../src/routes/controllers/meal.controller");
const home = require("../src/routes/controllers/studentenhuis.controller");
const database = require("../dao/database");
const faker = require("faker/locale/nl");

class Seed {
  async wipeData() {
    await database.execute("DELETE FROM `meal`");
    await database.execute("ALTER TABLE `meal` AUTO_INCREMENT = 1");
    await database.execute("DELETE FROM `studenthome`");
    await database.execute("ALTER TABLE `studenthome` AUTO_INCREMENT = 1");
  }
}
module.exports = new Seed();
