// process.env.PORT = 3001;
// process.env.NODE_ENV = "testing";

// const chai = require("chai");
// const chaiHttp = require("chai-http");
// const pool = require("../../dao/database");
// const app = require("../../server");
// const jwt = require("jsonwebtoken");

// chai.use(chaiHttp);

// describe("UC-301 Maaltijd aanmaken", function () {
//   it("TC-301-1 Verplicht veld ontbreekt", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//       .send({
//         userID: 16,
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         // price: 29,
//         maxParticipants: 5,
//       })
//       .end(async function (err, response) {
//         chai.expect(response).to.have.header("content-type", /json/);
//         chai.expect(response).status(400);
//       });
//   });

//   it("TC-301-2 Niet ingelogd", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .send({
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         price: 29,
//         maxParticipants: 5,
//       })
//       .end(async function (err, response) {
//         chai.expect(response).to.have.header("content-type", /json/);
//         chai.expect(response).status(401);
//       });
//   });

//   it("TC-301-3 Maaltijd succesvol toegevoegd ", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//       .send({
//         userID: 16,
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         price: 29,
//         maxParticipants: 5,
//       })
//       .end(async function (err, response) {
//         chai.expect(response).to.have.header("content-type", /json/);
//         chai.expect(response).status(200);
//       });
//   });
// });
