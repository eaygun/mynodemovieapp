// process.env.PORT = 3001;
// process.env.NODE_ENV = "testing";

// const chai = require("chai");
// const chaiHttp = require("chai-http");
// const pool = require("../../dao/database");
// const app = require("../../server");
// const jwt = require("jsonwebtoken");
// const seeder = require("../seed");

// chai.use(chaiHttp);

// describe("UC-305 Maaltijd verwijderen", function () {
//   beforeEach(async function () {
//     seeder.wipeData();
//   });

//   it("TC-305-4 Maaltijd bestaat niet ", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//       .send({
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         price: 29,
//         maxParticipants: 5,
//       })
//       .end(function (error, response) {
//         chai
//           .request(app)
//           .delete("/api/studenthome/1/meal/2")
//           .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//           .end(async function (err, response) {
//             chai.expect(response).to.have.header("content-type", /json/);
//             chai.expect(response).status(404);
//           });
//       });
//   });

//   it("TC-305-2 Niet ingelogd", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//       .send({
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         price: 29,
//         maxParticipants: 5,
//       })
//       .end(function (error, response) {
//         chai
//           .request(app)
//           .delete("/api/studenthome/1/meal/1")
//           .end(async function (err, response) {
//             chai.expect(response).to.have.header("content-type", /json/);
//             chai.expect(response).status(401);
//           });
//       });
//   });

//   it("TC-305-3 Actor is geen eigenaar ", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//       .send({
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         price: 29,
//         maxParticipants: 5,
//       })
//       .end(function (error, response) {
//         chai
//           .request(app)
//           .delete("/api/studenthome/1/meal/1")
//           .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//           .end(async function (err, response) {
//             chai.expect(response).to.have.header("content-type", /json/);
//             chai.expect(response).status(401);
//           });
//       });
//   });

//   it("TC-305-5 Maaltijd succesvol verwijderd", function () {
//     chai
//       .request(app)
//       .post("/api/studenthome/1/meal")
//       .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//       .send({
//         name: "frkandels",
//         description: "snack",
//         ingredients: "meat",
//         allergies: "contains curry",
//         offeredOn: "21-11-2020",
//         price: 29,
//         maxParticipants: 5,
//       })
//       .end(function (error, response) {
//         chai
//           .request(app)
//           .delete("/api/studenthome/1/meal/1")
//           .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
//           .end(async function (err, response) {
//             chai.expect(response).to.have.header("content-type", /json/);
//             chai.expect(response).status(200);
//           });
//       });
//   });
// });
