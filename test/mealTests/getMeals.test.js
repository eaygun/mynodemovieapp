// process.env.PORT = 3001;
// process.env.NODE_ENV = "testing";

// const chai = require("chai");
// const chaiHttp = require("chai-http");
// const pool = require("../../dao/database");
// const app = require("../../server");
// const jwt = require("jsonwebtoken");
// const seeder = require("../seed");

// chai.use(chaiHttp);

// describe("UC-303 Lijst van maaltijden opvragen, UC-304 Details van een maaltijd opvragen", function () {
//   beforeEach(function () {
//     seeder.wipeData();
//   });

//   it("TC-303-1 Lijst van maaltijden geretourneerd", function () {
//     chai
//       .request(app)
//       .get("/api/studenthome/1/meal")
//       .end(async function (err, response) {
//         chai.expect(response).to.have.header("content-type", /json/);
//         chai.expect(response).status(200);
//       });
//   });

//   it("TC-304-1 Maaltijd bestaat niet ", function () {
//     chai
//       .request(app)
//       .get("/api/studenthome/1/meal/2")
//       .end(async function (err, response) {
//         chai.expect(response).to.have.header("content-type", /json/);
//         chai.expect(response).status(404);
//       });
//   });

//   it("TC-304-2 Details van maaltijd geretourneerd", function () {
//     chai
//       .request(app)
//       .get("/api/studenthome/1/meal/1")
//       .end(async function (err, response) {
//         chai.expect(response).to.have.header("content-type", /json/);
//         chai.expect(response).status(200);
//       });
//   });
// });
