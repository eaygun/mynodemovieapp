process.env.PORT = 3001;
process.env.NODE_ENV = "testing";

const chai = require("chai");
const chaiHttp = require("chai-http");
const pool = require("../../dao/database");
const app = require("../../server");
const jwt = require("jsonwebtoken");

chai.use(chaiHttp);

describe("UC-101 Registreren", function () {
  it("TC-101-1 Verplicht veld ontbreekt", function () {
    chai
      .request(app)
      .post("/api/register")
      .send({
        firstname: "henk",
        lastname: "test",
        // StudentNr missing
        email: "xyz@domain.com",
        password: "secret",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });

  it("TC-101-2 Invalide email adres", function () {
    chai
      .request(app)
      .post("/api/register")
      .send({
        firstname: "henk",
        lastname: "test",
        studentNr: "2174213",
        email: "xyz@domain",
        password: "secretpass",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });

  it("TC-101-3 Invalide wachtwoord ", function () {
    chai
      .request(app)
      .post("/api/register")
      .send({
        firstname: "henk",
        lastname: "test",
        studentNr: "2174213",
        email: "xyz@domain.com",
        password: "secret",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });

  it("TC-101-5 Gebruiker succesvol geregistreerd", function () {
    chai
      .request(app)
      .post("/api/register")
      .send({
        firstname: "henk",
        lastname: "test",
        studentNr: "2174213",
        email: "henk@student.avans.nl",
        password: "secretpass",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(200);
      });
  });

  it("TC-101-4 Gebruiker bestaat al", function () {
    chai
      .request(app)
      .post("/api/register")
      .send({
        firstname: "henk",
        lastname: "test",
        studentNr: "2174213",
        email: "henk@student.avans.nl",
        password: "secretpass",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });
});
