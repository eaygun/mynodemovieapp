process.env.PORT = 3001;
process.env.NODE_ENV = "testing";

const chai = require("chai");
const chaiHttp = require("chai-http");
const pool = require("../../dao/database");
const app = require("../../server");
const jwt = require("jsonwebtoken");
const seeder = require("../seed");
const logger = require("tracer").console();
require("dotenv").config();

chai.use(chaiHttp);

describe("UC-204 Studentenhuis verwijderen", function () {
  it("TC-205-1 Studentenhuis bestaat niet", function () {
    chai
      .request(app)
      .delete("/api/studenthome/2")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(404);
      });
  });

  it("TC-205-2 Niet ingelogd", function () {
    chai
      .request(app)
      .delete("/api/studenthome/1")
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(401);
      });
  });

  it("TC-205-3 Actor is geen eigenaar ", function () {
    chai
      .request(app)
      .delete("/api/studenthome/1")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 8 }, `secret`))
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(401);
      });
  });

  it("TC-205-4 Studentenhuis succesvol verwijderd", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(async function (error, response) {
        chai
          .request(app)
          .delete("/api/studenthome/1")
          .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
          .end(async function (err, response) {
            chai.expect(response).to.have.header("content-type", /json/);
            chai.expect(response).status(200);
          });
      });
  });
});
