process.env.PORT = 3001;
process.env.NODE_ENV = "testing";

const chai = require("chai");
const chaiHttp = require("chai-http");
const pool = require("../../dao/database");
const app = require("../../server");
const jwt = require("jsonwebtoken");
const logger = require("tracer").console();
require("dotenv").config();

chai.use(chaiHttp);

describe("UC-201 maak studentenhuis", function () {
  it("TC-201-5 Niet ingelogd ", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(401);
      });
  });

  jwt.sign({ id: 1 }, "secret", { expiresIn: "2h" }, (err, token) => {
    it("TC-201-1 Verplicht veld ontbreekt", function () {
      chai
        .request(app)
        .post("/api/studenthome")
        .set("Authorization", `Bearer ` + token)
        .send({
          name: "Blaze powder",
          address: "Deez",
          houseNr: 23,
          postalCode: "4702KM",
          // Telephone missing
          city: "Roosendaal",
        })
        .end(async function (err, res) {
          chai.expect(response).to.have.header("content-type", /json/);
          chai.expect(response).status(400);
        });
    });
  });

  it("TC-201-2 Invalide postcode", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "470AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });

  it("TC-201-3 Invalide telefoonnummer", async function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702KM",
        telephone: "06123456",
        city: "Roosendaal",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });

  it("TC-201-6 Studentenhuis succesvol toegevoegd", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        ID: 1,
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(200);
      });
  });

  it("TC-201-4 studentenhuis bestaat al", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(async function (err, response) {
        chai.expect(response).to.have.header("content-type", /json/);
        chai.expect(response).status(400);
      });
  });
});
