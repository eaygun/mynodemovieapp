process.env.PORT = 3001;
process.env.NODE_ENV = "testing";

const chai = require("chai");
const chaiHttp = require("chai-http");
const pool = require("../../dao/database");
const app = require("../../server");
const jwt = require("jsonwebtoken");
const seeder = require("../seed");

chai.use(chaiHttp);
/**
 * Query om twee movies toe te voegen. Let op de UserId, die moet matchen
 * met de user die je ook toevoegt.
 */

describe("UC-204 Studentenhuis wijzigen", function () {
  it("TC-204-1 Verplicht veld ontbreekt", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(function (error, response) {
        chai
          .request(app)
          .put("/api/studenthome/1")
          .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
          .send({
            name: "Blaze powder",
            address: "Deez",
            houseNr: 23,
            postalCode: "4702AS",
            telephone: "0612345678",
            // Missing city
          })
          .end(async function (err, response) {
            chai.expect(response).to.have.header("content-type", /json/);
            chai.expect(response).status(400);
          });
      });
  });

  it("TC-204-2 Invalide postcode", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "470AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(async function (error, response) {
        chai
          .request(app)
          .put("/api/studenthome/1")
          .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
          .send({
            name: "Blaze powder",
            address: "Deez",
            houseNr: 23,
            postalCode: "470AS",
            telephone: "0612345678",
            city: "Roosendaal",
          })
          .end(function (err, response) {
            chai.expect(response).status(500);
          });
      });
  });

  it("TC-204-3 Invalide telefoonnummer", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(function (error, response) {
        chai
          .request(app)
          .put("/api/studenthome/1")
          .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
          .send({
            name: "Blaze powder",
            address: "Deez",
            houseNr: 23,
            postalCode: "4702AS",
            telephone: "0612345",
            city: "Roosendaal",
          })
          .end(function (err, response) {
            chai.expect(response).status(500);
          });
      });
  });

  it("TC-204-4 Studentenhuis bestaat niet", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        id: 1,
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(function (error, response) {
        chai
          .request(app)
          .put("/api/studenthome/2")
          .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
          .send({
            name: "Blaze powder",
            address: "Deez",
            houseNr: 23,
            postalCode: "4702AS",
            telephone: "0612345",
            city: "Roosendaal",
          })
          .end(async function (err, response) {
            chai.expect(response).status(500);
          });
      });
  });

  it("TC-204-5 Niet ingelogd", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(function (error, response) {
        chai
          .request(app)
          .put("/api/studenthome/1")
          .send({
            name: "Blaze powder",
            address: "Deez",
            houseNr: 23,
            postalCode: "4702AS",
            telephone: "0612345678",
            city: "Roosendaal",
          })
          .end(async function (err, response) {
            chai.expect(response).status(500);
          });
      });
  });

  it("TC-204-6 Studentenhuis succesvol gewijzigd", function () {
    chai
      .request(app)
      .post("/api/studenthome")
      .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
      .send({
        name: "Blaze powder",
        address: "Deez",
        houseNr: 23,
        postalCode: "4702AS",
        telephone: "0612345678",
        city: "Roosendaal",
      })
      .end(function (error, response) {
        chai
          .request(app)
          .put("/api/studenthome/1")
          .set("Authorization", `Bearer ` + jwt.sign({ id: 16 }, `secret`))
          .send({
            name: "Blaze powder",
            address: "Deez",
            houseNr: 23,
            postalCode: "4702AS",
            telephone: "0612345678",
            city: "Roosendaal",
          })
          .end(async function (err, response) {
            chai.expect(response).to.have.header("content-type", /json/);
            chai.expect(response).status(200);
          });
      });
  });
});
