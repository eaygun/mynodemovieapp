// process.env.PORT = 3001;
// process.env.NODE_ENV = "testing";

// process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb";
// process.env.NODE_ENV = "testing";
// process.env.LOGLEVEL = "error";

// const chai = require("chai");
// const chaiHttp = require("chai-http");
// const pool = require("../../dao/database");
// const app = require("../../server");
// const jwt = require("jsonwebtoken");
// const logger = require("tracer").console();
// const assert = require("assert");
// require("dotenv").config();
// chai.should();
// chai.use(chaiHttp);

// describe("UC-202 Overzicht van studentenhuizen", function () {
//   const CLEAR_DB = "DELETE IGNORE FROM `studenthome`";
//   const ADD_HOMES =
//     "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES ('1', 'Princenhage', 'Princenhage', 11, 1,'4706RX','061234567891','Breda'),('2', 'Haagdijk 23', 'Haagdijk', 4, 1, '4706RX','061234567891','Breda')";
//   const ADD_HOME_ADMIN =
//     "INSERT INTO `home_administrators` (`UserId`, `StudenthomeID`) VALUES (1, 1)";

//   it("TC-202-1 | Show 0 studentHomes return HTTP code 200 and an empty JSON object", (done) => {
//     chai
//       .request(app)
//       .get("/api/studenthome")
//       .end((err, res) => {
//         assert.ifError(err);
//         res.should.have.status(404);
//         res.should.be.an("object");

//         res.body.should.be.an("object").that.has.all.keys("result");

//         let { status, result } = res.body;
//         logger.debug(res.body);
//         result.should.be.a("array").that.has.lengthOf(2);

//         done();
//       });
//   });

//   it("TC-202-2 | Show 2 studentHomes return HTTP code 200 and JSON object with 2 studenthomes", (done) => {
//     chai
//       .request(app)
//       .get("/api/studenthome?city=Roosendaal")
//       .end((err, res) => {
//         assert.ifError(err);
//         res.should.have.status(404);

//         done();
//       });
//   });
//   // it("TC-202-3 | Show studentHomes on not existing city return HTTP code 404 and JSON object with errorMsg", (done) => {
//   //   chai
//   //     .request(app)
//   //     .get("/api/studenthome?city=asdasdasd")
//   //     .end((err, res) => {
//   //       assert.ifError(err);
//   //       res.should.have.status(404);
//   //       res.should.be.an("object");

//   //       done();
//   //     });
//   // });
//   // it("TC-202-4 | Show studentHomes on not existing name return HTTP code 404 and JSON object with errorMsg", (done) => {
//   //   chai
//   //     .request(app)
//   //     .get("/api/studenthome?name=asdasdasd")
//   //     .end((err, res) => {
//   //       assert.ifError(err);
//   //       res.should.have.status(404);
//   //       res.should.be.an("object");

//   //       done();
//   //     });
//   // });
//   it("TC-202-5 | Show studentHomes on existing city return HTTP code 200 and JSON object with the found studentHome", (done) => {
//     chai
//       .request(app)
//       .get("/api/studenthome?city=Breda")
//       .end((err, res) => {
//         assert.ifError(err);
//         res.should.have.status(404);
//         res.should.be.an("object");

//         res.body.should.be.an("object").that.has.all.keys("result");

//         let { status, result } = res.body;
//         result.should.be.a("array");

//         // Check if every object contains the given city
//         result.forEach((studentHome) => {
//           studentHome.should.be.an("Object").that.contains({ City: "Breda" });
//         });

//         done();
//       });
//   });
//   it("TC-202-6 | Show studentHomes on existing name return HTTP code 200 and JSON object with the found studentHome", (done) => {
//     chai
//       .request(app)
//       .get("/api/studenthome?name=Princenhage")
//       .end((err, res) => {
//         assert.ifError(err);
//         res.should.have.status(404);
//         res.should.be.an("object");

//         res.body.should.be.an("object").that.has.all.keys("result");

//         let { status, result } = res.body;
//         result.should.be.a("array");

//         // Check if every object contains the given name
//         result.forEach((studentHome) => {
//           studentHome.should.be
//             .an("Object")
//             .that.contains({ Name: "Princenhage" });
//         });

//         done();
//       });
//   });

//   it("TC-203-1 | StudentHomeId does not exist return HTTP code 404 and JSON object with errorMsg", (done) => {
//     chai
//       .request(app)
//       .get("/api/studenthome/1000")
//       .end((err, res) => {
//         assert.ifError(err);
//         res.should.have.status(404);
//         res.should.be.an("object");

//         done();
//       });
//   });
//   it("TC-203-2 | StudentHomeId does exist return HTTP code 200 and JSON object with information of studentHome", (done) => {
//     chai
//       .request(app)
//       .get("/api/studenthome/1")
//       .end((err, res) => {
//         assert.ifError(err);
//         res.should.have.status(200);
//         res.should.be.an("object");

//         res.body.should.be.an("object").that.has.all.keys("result");

//         done();
//       });
//   });
// });
