//
// Authentication controller
//
const { assert } = require("chai");
const jwt = require("jsonwebtoken");
const pool = require("../../../dao/database");
const logger = require("tracer").console();
const jwtSecretKey = require("../../../dao/database.config").jwtSecretKey;

module.exports = {
  login(req, res, next) {
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      if (connection) {
        // 1. Kijk of deze useraccount bestaat.
        connection.query(
          "SELECT `ID`, `Email`, `Password`, `Student_Number`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?",
          [req.body.email],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.debug("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString(),
              });
            } else {
              // 2. Er was een resultaat, check het password.
              logger.info("Result from database: ");
              logger.info(rows);
              if (
                rows &&
                rows.length === 1 &&
                rows[0].Password == req.body.password
              ) {
                logger.info("passwords DID match, sending valid token");
                // Create an object containing the data we want in the payload.
                const payload = {
                  id: rows[0].ID,
                };
                // Userinfo returned to the caller.
                const userinfo = {
                  status: "succes, welcome to the samen-eten-server!",
                  id: rows[0].ID,
                  Name: rows[0].First_Name + " " + rows[0].Last_Name,
                  token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
                };
                logger.debug("Logged in, sending: ", userinfo);
                res.status(200).json(userinfo);
              } else {
                logger.info("User not found or password invalid");
                res.status(401).json({
                  message: "User not found or password invalid",
                  datetime: new Date().toISOString(),
                });
              }
            }
          }
        );
      }
    });
  },

  //
  //
  //
  validateLogin(req, res, next) {
    // Verify that we receive the expected input
    logger.debug("Called validateLogin");
    try {
      const { email, password } = req.body;

      assert(typeof email === "string", "email is invalid or missing.");
      assert(typeof password === "string", "password is invalid or missing.");

      // Check if email is valid dutch format: tekst@hotmail.com
      const rege = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      assert(rege.test(email), "email was invalid");

      // Check if password has length between 7 and 14
      const passw = /^[A-Za-z]\w{7,14}$/;
      assert(passw.test(password), "password was invalid");

      logger.debug("data is valid");
      next();
    } catch (err) {
      logger.error("data is Invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  //
  //
  //
  register(req, res, next) {
    logger.info("register");

    /**
     * Query the database to see if the email of the user to be registered already exists.
     */
    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool: " + err.toString());
        res.status(500).json({
          error: err.toString(),
          datetime: new Date().toISOString(),
        });
      }
      if (connection) {
        let { firstname, lastname, email, studentNr, password } = req.body;

        connection.query(
          "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)",
          [firstname, lastname, email, 12345, password],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              // When the INSERT fails, we assume the user already exists
              logger.debug("Error: " + err.toString());
              res.status(400).json({
                message: "This email has already been taken.",
                datetime: new Date().toISOString(),
              });
            } else {
              logger.trace(rows);
              // Create an object containing the data we want in the payload.
              // This time we add the id of the newly inserted user
              const payload = {
                id: rows.insertId,
              };
              // Userinfo returned to the caller.
              const userinfo = {
                status: "succesfully registered in the samen-eten-server!",
                id: rows.insertId,
                firstName: firstname,
                lastName: lastname,
                studentNr: studentNr,
                emailAdress: email,
                token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
              };
              logger.debug("Registered", userinfo);
              res.status(200).json(userinfo);
            }
          }
        );
      }
    });
  },

  //
  //
  //
  validateRegister(req, res, next) {
    logger.debug("Called validateRegister");
    // Verify that we receive the expected input
    try {
      const { firstname, lastname, studentNr, email, password } = req.body;

      assert(typeof firstname === "string", "firstname is invalid or missing.");
      assert(typeof lastname === "string", "lastname is invalid or missing.");
      assert(typeof studentNr === "string", "studentNr is invalid or missing.");
      assert(typeof email === "string", "email is invalid or missing.");
      assert(typeof password === "string", "password is invalid or missing.");

      // Check if email is valid dutch format: tekst@hotmail.com
      const rege = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
      assert(rege.test(email), "email was invalid");

      // Check if studentNr is valid Avans format: 2174757
      const studentRege = /^[0-9]{7}/;
      assert(studentRege.test(studentNr), "studentNr was invalid");
      // Check if password has length between 7 and 14
      const passw = /^[A-Za-z]\w{7,14}$/;
      assert(passw.test(password), "password was invalid");

      logger.debug("data is valid");
      next();
    } catch (err) {
      logger.error("data is Invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  //
  //
  //
  validateToken(req, res, next) {
    logger.info("validateToken called");
    // logger.trace(req.headers)
    // The headers should contain the authorization-field with value 'Bearer [token]'
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      logger.warn("Authorization header missing!");
      res.status(401).json({
        error: "Authorization header missing!",
        datetime: new Date().toISOString(),
      });
    } else {
      // Strip the word 'Bearer ' from the headervalue
      const token = authHeader.substring(7, authHeader.length);

      jwt.verify(token, jwtSecretKey, (err, payload) => {
        if (err) {
          logger.warn("Not authorized");
          res.status(401).json({
            error: "Not authorized",
            datetime: new Date().toISOString(),
          });
        }
        if (payload) {
          logger.debug("token is valid", payload);
          // User heeft toegang. Voeg UserId uit payload toe aan
          // request, voor ieder volgend endpoint.
          req.userId = payload.id;
          next();
        }
      });
    }
  },

  renewToken(req, res, next) {
    logger.debug("renewToken");

    pool.getConnection((err, connection) => {
      if (err) {
        logger.error("Error getting connection from pool");
        res
          .status(500)
          .json({ error: err.toString(), datetime: new Date().toISOString() });
      }
      if (connection) {
        // 1. Kijk of deze useraccount bestaat.
        connection.query(
          "SELECT * FROM `user` WHERE `ID` = ?",
          [req.userId],
          (err, rows, fields) => {
            connection.release();
            if (err) {
              logger.error("Error: ", err.toString());
              res.status(500).json({
                error: err.toString(),
                datetime: new Date().toISOString(),
              });
            } else {
              // 2. User gevonden, return user info met nieuw token.
              // Create an object containing the data we want in the payload.
              const payload = {
                id: rows[0].ID,
              };
              // Userinfo returned to the caller.
              const userinfo = {
                id: rows[0].ID,
                firstName: rows[0].First_Name,
                lastName: rows[0].Last_Name,
                emailAdress: rows[0].Email,
                token: jwt.sign(payload, jwtSecretKey, { expiresIn: "2h" }),
              };
              logger.debug("Sending: ", userinfo);
              res.status(200).json(userinfo);
            }
          }
        );
      }
    });
  },

  getApiInfo(req, res, next) {
    logger.info("GetApiInfo called!");
    const userId = req.userId;

    let sqlQuery =
      "SELECT First_Name, Last_Name, Student_Number FROM user WHERE ID = " +
      userId;
    logger.debug("[studenthuis.controller] sqlQuery = ", sqlQuery);
    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(404).json({
          message: "GetById failed to connect!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetById failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
                description: "This is a nodejs-samen-eten-server",
                sonarqubeUrl:
                  "https://sonarqube.avans-informatica-breda.nl/dashboard?id=samen-eten-server-nodejs",
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },
};
