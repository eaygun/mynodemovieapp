const { assert } = require("chai");
const database = require("../../../dao/database");
const pool = require("../../../dao/database");
const logger = require("tracer").console();

let controller = {
  signIntoMeal(req, res, next) {
    logger.info("signIntoMeal called!");
    const userId = req.userId;
    const studenthomeID = req.params.homeId;
    const mealId = req.params.mealId;

    let sqlQuery =
      "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?, ?, ?, ?)";
    logger.debug("sqlQuery = ", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("Error getting connection: ", err);
        res.status(400).json({
          message: "signIntoMeal failed getting connection!",
          error: err,
        });
      }

      if (connection) {
        // Use the connection
        connection.query(
          sqlQuery,
          [userId, studenthomeID, mealId, new Date()],
          (error, results, fields) => {
            logger.debug(userId, studenthomeID, mealId, new Date());
            // When done with the connection, release it.
            connection.release();
            // Handle error after the release.
            if (error) {
              logger.error("error", error.toString());
              res.status(400).json({
                message: "signIntoMeal failed calling query",
                error: error.toString(),
              });
            }
            if (results) {
              logger.trace("results: ", results);
              res.status(200).json({
                result: {
                  UserId: userId,
                  StudenthomeID: studenthomeID,
                  MealId: mealId,
                  SignedUpOn: new Date(),
                },
              });
            }
          }
        );
      }
    });
  },

  signOffFromMeal(req, res, next) {
    logger.info("signOffFromMeal called!");
    const userId = req.userId;
    const studenthomeID = req.params.homeId;
    const mealId = req.params.mealId;

    let sqlQuery =
      "DELETE FROM participants WHERE UserID = " +
      userId +
      " AND StudenthomeID = " +
      studenthomeID +
      " AND MealID = " +
      mealId;
    logger.debug("sqlQuery = ", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("Error getting connection: ", err);
        res.status(400).json({
          message: "signOffFromMeal failed getting connection!",
          error: err,
        });
      }

      if (connection) {
        // Use the connection
        connection.query(sqlQuery, [], (error, results, fields) => {
          logger.debug(userId, studenthomeID, mealId);
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if (error) {
            logger.error("error", error.toString());
            res.status(400).json({
              message: "signOffFromMeal failed calling query",
              error: error.toString(),
            });
          }
          if (results) {
            logger.trace("results: ", results);
            res.status(200).json({
              deleted: {
                UserId: userId,
                StudenthomeID: studenthomeID,
                MealId: mealId,
              },
            });
          }
        });
      }
    });
  },

  getListOfParticipantsFromMeal(req, res, next) {
    logger.info("getListOfParticipantsFromMeal called!");
    const mealId = req.params.mealId;
    logger.debug("getListOfParticipantsFromMeal", "mealID = ", mealId);

    let sqlQuery = "SELECT * FROM participants WHERE MealID = " + mealId;

    logger.debug("getById", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(404).json({
          message: "GetById failed to connect!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetById failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },

  getDetailsFromParticipant(req, res, next) {
    logger.info("getDetailsFromParticipant called!");
    const mealId = req.params.mealId;
    const userId = req.params.participantId;
    logger.debug(
      "getDetailsFromParticipant",
      "mealID = ",
      mealId,
      " userId = ",
      userId
    );

    let sqlQuery =
      "SELECT * FROM participants WHERE MealID = " +
      mealId +
      " AND UserID = " +
      userId;

    logger.debug("getDetailsFromParticipant", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(404).json({
          message: "getDetailsFromParticipant failed to connect!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "getDetailsFromParticipant failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },
};

module.exports = controller;
