const database = require("../../../dao/database");
const pool = require("../../../dao/database");
var logger = require("tracer").console();
const { assert } = require("chai");

let controller = {
  validateMeal(req, res, next) {
    logger.debug("Called validateMeal");
    try {
      const {
        name,
        description,
        ingredients,
        allergies,
        price,
        maxParticipants,
      } = req.body;

      // Check if value is a valid type
      assert(typeof name === "string", "name is invalid or missing");
      assert(typeof description === "string", "street is invalid or missing");
      assert(typeof price === "number", "price is invalid or missing");
      assert(
        typeof ingredients === "string",
        "ingredients is invalid or missing"
      );
      assert(typeof allergies === "string", "allergies is invalid or missing");
      assert(typeof maxParticipants === "number", "city is invalid or missing");

      logger.debug("Meal is valid");
      next();
    } catch (err) {
      logger.error("Meal data is Invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  createMeal(req, res, next) {
    logger.info("createMeal called!");
    const homeId = req.params.homeId;
    const userId = req.userId;
    const meal = req.body;
    const createdOn = new Date();
    let {
      name,
      description,
      ingredients,
      allergies,
      OfferedOn,
      price,
      maxParticipants,
    } = meal;
    logger.trace("meal = ", meal);

    let sqlQuery =
      "INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    logger.debug("sqlQuery = ", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("Error getting connection: ", err);
        res.status(400).json({
          message: "creating Meal failed getting connection!",
          error: err,
        });
      }

      if (connection) {
        // Use the connection
        connection.query(
          sqlQuery,
          [
            name,
            description,
            ingredients,
            allergies,
            createdOn,
            OfferedOn,
            parseInt(price, 10),
            userId,
            homeId,
            maxParticipants,
          ],
          (error, results, fields) => {
            logger.debug(
              name,
              description,
              ingredients,
              allergies,
              createdOn,
              OfferedOn,
              price,
              userId,
              homeId,
              maxParticipants
            );
            // When done with the connection, release it.
            connection.release();
            // Handle error after the release.
            if (error) {
              logger.error("error", error.toString());
              res.status(400).json({
                message: "Creating meal failed calling query",
                error: error.toString(),
              });
            }
            if (results) {
              logger.trace("results: ", results);
              res.status(200).json({
                result: {
                  mealId: results.insertId,
                  studenthomeID: homeId,
                  createdOn: createdOn,
                  ...meal,
                },
              });
            }
          }
        );
      }
    });
  },

  updateMeal(req, res) {
    const mealId = req.params.mealId;
    const houseId = req.params.homeId;
    const meal = req.body;
    logger.debug("update", "id = ", mealId, "houseId = ", houseId);

    let {
      name,
      description,
      ingredients,
      allergies,
      OfferedOn,
      price,
      maxParticipants,
    } = meal;
    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "delete failed!",
          error: err,
        });
      }

      let sqlQuery =
        "UPDATE meal SET meal.Name = ?, " +
        "meal.Description = ?, " +
        "meal.Ingredients = ?, " +
        "meal.Allergies = ?, " +
        "meal.OfferedOn = ?, " +
        "meal.Price = ?, " +
        "meal.StudenthomeID = ?, " +
        "meal.MaxParticipants = ? " +
        "WHERE meal.ID = " +
        mealId +
        " AND meal.StudenthomeID = " +
        houseId;

      connection.query(
        sqlQuery,
        [
          name,
          description,
          ingredients,
          allergies,
          OfferedOn,
          price,
          houseId,
          maxParticipants,
        ],
        (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if (error) {
            res.status(400).json({
              message: "Could not updated item",
              error: error,
            });
          }
          if (results) {
            if (results.affectedRows === 0) {
              logger.trace("item was NOT updated");
              res.status(401).json({
                result: {
                  error:
                    "Item not found or you do not have access to this item",
                },
              });
            } else {
              logger.trace("item was updated");
              res.status(200).json({
                status: "successfully updated item",
              });
            }
          }
        }
      );
    });
  },

  getMeals(req, res, next) {
    const mealId = req.params.mealId;
    const houseId = req.params.homeId;

    let sqlQuery =
      "SELECT * FROM meal WHERE ID = " +
      mealId +
      " AND StudenthomeID = " +
      houseId;

    logger.debug("getById", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "delete failed!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetById failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },

  deleteMeal(req, res, next) {
    const userId = req.userId;
    const houseId = req.params.homeId;
    const mealId = req.params.mealId;

    logger.debug(
      "delete",
      "houseid = ",
      houseId,
      "userid = ",
      userId,
      "mealId = ",
      mealId
    );

    let sqlQuery =
      "DELETE FROM meal WHERE ID = " +
      mealId +
      " AND UserID = " +
      userId +
      " AND StudenthomeID = " +
      houseId;

    logger.debug("deleteMeal", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "delete failed!",
          error: err,
        });
      }

      connection.query(sqlQuery, (error, results, fields) => {
        // When done with the connection, release it.
        connection.release();
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            message: "Could not delete item",
            error: error,
          });
        }
        if (results) {
          if (results.affectedRows === 0) {
            logger.trace("item was NOT deleted");
            res.status(401).json({
              result: {
                error: "Item not found or you do not have access to this item",
              },
            });
          } else {
            logger.trace("item was deleted");
            res.status(200).json({
              status: "successfully deleted item",
            });
          }
        }
      });
    });
  },

  getListOfMeals(req, res, next) {
    const houseId = req.params.homeId;

    logger.debug("getListOfMeals", "id = ", houseId);

    let sqlQuery = "SELECT * FROM meal WHERE StudenthomeID = " + houseId;

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "delete failed!",
          error: err,
        });
      }

      connection.query(sqlQuery, (error, results, fields) => {
        // When done with the connection, release it.
        connection.release();
        // Handle error after the release.
        if (error) {
          res.status(400).json({
            message: "Could not delete item",
            error: error,
          });
        }
        if (results) {
          logger.trace("results: ", results);
          const mappedResults = results.map((item) => {
            return {
              ...item,
            };
          });
          res.status(200).json({
            result: mappedResults,
          });
        }
      });
    });
  },
};

module.exports = controller;
