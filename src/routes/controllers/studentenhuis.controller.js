const { assert } = require("chai");
const database = require("../../../dao/database");
const pool = require("../../../dao/database");
const logger = require("tracer").console();

let controller = {
  validateStudentHome(req, res, next) {
    logger.debug("Called validateStudentHome");
    try {
      const { name, address, houseNr, postalCode, telephone, city } = req.body;

      // Check if value is a valid type
      assert(typeof name === "string", "name is invalid or missing");
      assert(typeof address === "string", "address is invalid or missing");
      assert(typeof houseNr === "number", "houseNr is invalid or missing");
      assert(
        typeof postalCode === "string",
        "postalCode is invalid or missing"
      );
      assert(typeof telephone === "string", "telephone is invalid or missing");
      assert(typeof city === "string", "city is invalid or missing");

      // Check if postalCode is valid dutch format: 0000AA
      const rege = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
      assert(rege.test(postalCode), "postalCode was invalid");

      // Check phoneNr valid formats:
      // (123) 456-7890 | (123)456-7890 | 123-456-7890 | 123.456.7890 | 1234567890 | +31636363634 | 075-63546725
      const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
      assert(re.test(telephone), "telephone was invalid");

      logger.debug("StudentHome is valid");
      next();
    } catch (err) {
      logger.error("StudentHome data is Invalid: ", err.message);
      next({ message: err.message, errCode: 400 });
    }
  },

  createStudentHouse(req, res, next) {
    logger.info("CreateStudentHouse called!");
    const studentHouse = req.body;

    let { name, address, houseNr, postalCode, telephone, city } = studentHouse;

    // Zolang er geen account is geconnect op 1, anders req.userId. Dit is een voorlopige oplossing
    const userId = req.userId;

    let sqlQuery =
      "INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?, ?, ?, ?, ?, ?, ?)";

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("Error getting connection: ", err);
        res.status(500).json({
          message: "creating Studenthome failed getting connection!",
          error: err,
        });
      }

      if (connection) {
        // Use the connection
        connection.query(
          sqlQuery,
          [
            name,
            address,
            parseInt(houseNr, 10),
            userId,
            postalCode,
            telephone,
            city,
          ],
          (error, results, fields) => {
            logger.debug(
              name,
              address,
              houseNr,
              userId,
              postalCode,
              telephone,
              city
            );
            // When done with the connection, release it.
            connection.release();
            // Handle error after the release.
            if (error) {
              logger.error("error", error.toString());
              res.status(400).json({
                message: "Creating studenthome failed calling query",
                error: error.toString(),
              });
            }
            if (results) {
              // Toevoegen aan administrators
              logger.trace("results: ", results);
              res.status(200).json({
                result: {
                  id: results.insertId,
                  userID: userId,
                  ...studentHouse,
                },
              });
            }
          }
        );
      }
    });
  },

  getStudentHouses(req, res, next) {
    logger.trace("getAll called!");
    // We willen de user info bij iedere film ophalen; we doen dat via een JOIN.
    let sqlQuery = "SELECT * FROM studenthome";

    // We willen alle query params meenemen in de SQL query, door de key/values in de SQL
    // query toe te voegen. We kunnen de afzonderlijke keys als properties uitlezen, zoals:
    // const studioname = req.query.studio

    // Handiger/mooier:
    // Express levert req.query als een object met key/values, bv { name: 'Pixar', ...: ...}
    // Om te kunnen itereren maken we een array van het object met Object.entries
    const queryParams = Object.entries(req.query);
    // queryParmas is nu een array van arrays: [['name', 'Pixar'], ['...', ''...']]
    // Dit array kunnen we nu via arrayfuncties zoals map() en reduce() doorlopen.
    logger.info("queryParams:", queryParams);
    if (queryParams.length > 0) {
      let queryString = queryParams
        .map((param) => {
          // map maakt een nieuwe waarde van gegeven invoer; hier een string van twee arrayvalues.
          return `${param[0]} = '${param[1]}'`;
        })
        .reduce((a, b) => {
          // reduce 'reduceert' twee opeenvolgende waarden tot één eindwaarde.
          return `${a} AND ${b}`;
        });
      logger.info("queryString:", queryString);
      sqlQuery += ` WHERE ${queryString};`;
    }

    logger.debug("getAll", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        console.log(err);
        res.status(400).json({
          message: "GetAll failed!",
          error: err.toString(),
        });
      }
      if (connection) {
        // Use the connection
        // Merk op dat we door de map/reduce aanpaak nu geen prepared statement (met value = ?) hebben!
        // Het zou nog mooier zijn wanneer we dat wél via de map/reduce zouden doen. Dat kan; zoek zelf uit hoe!
        connection.query(sqlQuery, (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if (error) {
            res.status(404).json({
              message: "GetAll failed!",
              error: error,
            });
          }
          if (results) {
            logger.trace("results: ", results);
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            res.status(200).json({
              result: mappedResults,
            });
          }
        });
      }
    });
  },

  getStudentHouseDetails(req, res, next) {
    const houseId = req.params.homeId;
    logger.trace("getStudentHouseDetails called!");
    // We willen de user info bij iedere film ophalen; we doen dat via een JOIN.
    let sqlQuery =
      "SELECT " +
      "studenthome.ID," +
      "studenthome.Name," +
      "studenthome.Address," +
      "studenthome.House_Nr," +
      "studenthome.Postal_Code," +
      "studenthome.Telephone," +
      "studenthome.City," +
      "user.ID as UserID," +
      "user.First_Name," +
      "user.Last_Name " +
      "FROM studenthome " +
      "LEFT JOIN user ON studenthome.UserID = user.ID " +
      "WHERE studenthome.ID = " +
      houseId;

    logger.debug("getById", "sqlQuery =", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(404).json({
          message: "GetById failed to connect!",
          error: err,
        });
      }
      if (connection) {
        connection.query(sqlQuery, (error, results, fields) => {
          connection.release();
          if (error) {
            res.status(400).json({
              message: "GetById failed!",
              error: error,
            });
          }
          if (results) {
            const mappedResults = results.map((item) => {
              return {
                ...item,
              };
            });
            if (results.length < 1) {
              logger.trace("item was NOT found");
              res.status(404).json({
                result: {
                  error:
                    "Item not found or you do not have access to this item",
                },
              });
            } else {
              logger.trace("results: ", results);
              res.status(200).json({
                result: mappedResults,
              });
            }
          }
        });
      }
    });
  },

  updateStudentHouse(req, res, next) {
    const homeId = req.params.homeId;

    logger.debug("update", "id = ", homeId);

    let { name, address, houseNr, postalCode, telephone, city } = req.body;

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "update failed!",
          error: err,
        });
      }

      let sqlQuery =
        "UPDATE studenthome SET studenthome.Name = ?, " +
        "studenthome.Address = ?, " +
        "studenthome.House_Nr = ?, " +
        "studenthome.Postal_Code = ?, " +
        "studenthome.Telephone = ?, " +
        "studenthome.City = ? " +
        "WHERE studenthome.ID = " +
        homeId;
      connection.query(
        sqlQuery,
        [name, address, houseNr, postalCode, telephone, city],
        (error, results, fields) => {
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if (error) {
            res.status(400).json({
              message: "Could not updated item",
              error: error,
            });
          }
          if (results) {
            if (results.affectedRows === 0) {
              logger.trace("item was NOT updated");
              res.status(401).json({
                result: {
                  error:
                    "Item not found or you do not have access to this item",
                },
              });
            } else {
              logger.trace("item was updated");
              res.status(200).json({
                status: "successfully updated item",
              });
            }
          }
        }
      );
    });
  },

  deleteStudentHouse(req, res, next) {
    logger.info("deleteStudentHouse called!");
    const userId = req.userId;
    const studenthomeID = req.params.homeId;

    let sqlQuery =
      "DELETE FROM studenthome WHERE ID = " +
      studenthomeID +
      " AND UserID = " +
      userId;

    logger.debug("sqlQuery = ", sqlQuery);

    pool.getConnection(function (err, connection) {
      if (err) {
        logger.error("Error getting connection: ", err);
        res.status(400).json({
          message: "deleteStudentHouse failed getting connection!",
          error: err,
        });
      }

      if (connection) {
        // Use the connection
        connection.query(sqlQuery, [], (error, results, fields) => {
          logger.debug(studenthomeID, userId);
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if (error) {
            logger.error("error", error.toString());
            res.status(400).json({
              message: "deleteStudentHouse failed calling query",
              error: error.toString(),
            });
          }
          if (results) {
            if (results.affectedRows === 0) {
              logger.trace("item was NOT updated");
              res.status(401).json({
                result: {
                  error:
                    "Item not found or you do not have access to this item",
                },
              });
            } else {
              logger.trace("item was updated");
              logger.trace("results: ", results);
              res.status(200).json({
                deleted: {
                  UserId: userId,
                  StudenthomeID: studenthomeID,
                },
              });
            }
          }
        });
      }
    });
  },

  addUserToStudentHouse(req, res, next) {
    logger.info("addUserToStudentHouse called");
  },

  getStudentHouseByNameAndCity(req, res, next) {
    logger.info("GetStudentHouseByNameAndCity called");

    const houseName = req.params.name;
    const city = req.params.city;

    logger.debug(
      "GetStudentHouseByNameAndCity",
      "name = ",
      houseName,
      "city = ",
      city
    );

    pool.getConnection(function (err, connection) {
      if (err) {
        res.status(400).json({
          message: "GetStudentHouseByNameAndCity failed!",
          error: err,
        });

        let sqlQuery =
          "GET * FROM studenthome WHERE Name = " +
          houseName +
          " OR City = " +
          city;

        if (connection) {
          connection.query(sqlQuery, (error, results, fields) => {
            connection.release();
            if (error) {
              res.status(404).json({
                message: "getStudentHouseByNameAndCity failed!",
                error: error,
              });
            }
            if (results) {
              if (results.length < 1) {
                logger.trace("item was NOT updated");
                res.status(404).json({
                  result: {
                    error:
                      "Item not found or you do not have access to this item",
                  },
                });
              } else {
                const mappedResults = results.map((item) => {
                  return {
                    ...item,
                  };
                });
                logger.trace("results: ", results);
                res.status(200).json({
                  result: mappedResults,
                });
              }
            }
          });
        }
      }
    });
  },
};

module.exports = controller;
