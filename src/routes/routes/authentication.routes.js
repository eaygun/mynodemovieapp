//
// Authentication routes
//

const routes = require("express").Router();
const AuthController = require("../controllers/authentication.controller");

// UC-102 Login
routes.post("/login", AuthController.validateLogin, AuthController.login);

// UC-101 Registreren
routes.post(
  "/register",
  AuthController.validateRegister,
  AuthController.register
);
// Renew Token
routes.get(
  "/validate",
  AuthController.validateToken,
  AuthController.renewToken
);

// UC-103 Systeeminfo opvragen
routes.get("/info", AuthController.validateToken, AuthController.getApiInfo);

module.exports = routes;
