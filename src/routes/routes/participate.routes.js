const express = require("express");
const router = express.Router();
const controller = require("../controllers/participate.controller");
const AuthController = require("../controllers/authentication.controller");

// UC-401 Aanmelden voor maaltijd
router.post(
  "/studenthome/:homeId/meal/:mealId/signup",
  AuthController.validateToken,
  controller.signIntoMeal
);

// UC-402 Afmelden voor maaltijd
router.delete(
  "/studenthome/:homeId/meal/:mealId/signoff",
  AuthController.validateToken,
  controller.signOffFromMeal
);

// UC-403 Lijst van deelnemers opvragen
router.get(
  "/meal/:mealId/participants",
  AuthController.validateToken,
  controller.getListOfParticipantsFromMeal
);

//UC-404 details van deelnemer opvragen
router.get(
  "/meal/:mealId/participants/:participantId",
  AuthController.validateToken,
  controller.getDetailsFromParticipant
);
module.exports = router;
