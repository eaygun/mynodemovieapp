const express = require("express");
const router = express.Router();
var logger = require("tracer").console();

// Kijkt of de link bestaat, als het niet bestaat gaat het naar regel 62
router.all("*", (req, res, next) => {
  const reqMethod = req.method;
  const reqUrl = req.url;
  logger.info("Endpoint called: " + reqMethod + " " + reqUrl);
  next();
});

// Error handler
router.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send("Something broke!");
});

// Als link niet bestaat
router.all("*", (req, res) => {
  logger.info("Catch-all endpoint aangeroepen");
  const error = {
    message: "Endpoint does not exist",
  };
  res.status(404).json(error).end();
});

// 404 error handling (moet altijd onderaan)
router.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!");
});

module.exports = router;
