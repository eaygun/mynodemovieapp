const express = require("express");
const router = express.Router();
const controller = require("../controllers/meal.controller");
const AuthController = require("../controllers/authentication.controller");

// UC-301 Maaltijd aanmaken
router.post(
  "/:homeId/meal",
  AuthController.validateToken,
  controller.validateMeal,
  controller.createMeal
);

// UC-302 Maaltijd wijzigen
router.put(
  "/:homeId/meal/:mealId",
  AuthController.validateToken,
  controller.validateMeal,
  controller.updateMeal
);

// UC-303 Lijst van maaltijden opvragen
router.get("/:homeId/meal", controller.getListOfMeals);

// UC-304 Details van een maaltijd opvragen
router.get("/:homeId/meal/:mealId", controller.getMeals);

// UC-305 Maaltijd verwijderen
router.delete(
  "/:homeId/meal/:mealId",
  AuthController.validateToken,
  controller.deleteMeal
);

module.exports = router;
