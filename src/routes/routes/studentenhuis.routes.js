const express = require("express");
const router = express.Router();
const controller = require("../controllers/studentenhuis.controller");
const AuthController = require("../controllers/authentication.controller");

// Krijg alle studentenhuizen
router.get("/studenthome", controller.getStudentHouses);

// UC-201 Maak studentenhuis
router.post(
  "/studenthome",
  AuthController.validateToken,
  controller.validateStudentHome,
  controller.createStudentHouse
);

// UC-202 Overzicht van studentenhuizen
router.get("/studenthome/", controller.getStudentHouseByNameAndCity);

// UC-203 Details van studentenhuis
router.get("/studenthome/:homeId", controller.getStudentHouseDetails);

// UC-204 Studentenhuis wijzigen
router.put(
  "/studenthome/:homeId",
  AuthController.validateToken,
  controller.validateStudentHome,
  controller.updateStudentHouse
);

// UC-205 Studentenhuis verwijderen
router.delete(
  "/studenthome/:homeId",
  AuthController.validateToken,
  controller.deleteStudentHouse
);

// UC-206 Gebruiker toevoegen aan studentenhuis
router.put("/studenthome/:homeId/user", controller.addUserToStudentHouse);

module.exports = router;
