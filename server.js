const express = require("express");
const studentRoutes = require("./src/routes/routes/studentenhuis.routes");
const mealRoutes = require("./src/routes/routes/meal.routes");
const authenticationRoutes = require("./src/routes/routes/authentication.routes");
const participantsRoutes = require("./src/routes/routes/participate.routes");
const database = require("./dao/database");
const port = process.env.PORT || 3000;
const logger = require("tracer").console();
const cors = require("cors");
const app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use(cors());
app.use(express.json());
app.use("/api", authenticationRoutes);
app.use("/api", studentRoutes);
app.use("/api/studenthome", mealRoutes);
app.use("/api", participantsRoutes);

app.all("*", (req, res, next) => {
  res.status(404).json({
    error: "Endpoint does not exist!",
  });
});

app.listen(port, () => {
  logger.info(`Server running at http://localhost:${port}`);
});

module.exports = app;
