const faker = require("faker/locale/nl");
const timeToWait = 800;
const logger = require("tracer").console();
const mysql = require("mysql");
const dbconfig = require("./database.config").dbconfig;
const pool = mysql.createPool(dbconfig);

pool.on("connection", function (connection) {
  logger.trace("Database connection established");
});

pool.on("acquire", function (connection) {
  logger.trace("Database connection aquired");
});

pool.on("release", function (connection) {
  logger.trace("Database connection released");
});

module.exports = pool;
